// Barak Gonen 2019
// Skeleton code - inject DLL to a running process
#include <windows.h>
#include <tlhelp32.h> 
#include <stdio.h>
#define LIBRARY "C:\\Users\\magshimim\\source\\repos\\AssemblyProject\\x64\\Release\\Dll.dll"
#define BUFSIZE 100
int main()
{
	printf("Starting the dll injection...\n");
	HANDLE proc;
	DWORD err;
	char buffer[BUFSIZE];
	// Get full path of DLL to inject
	DWORD pathLen = GetFullPathNameA((LPCSTR)LIBRARY,BUFSIZE,buffer,NULL);
	
	// Get LoadLibrary function address �
	// the address doesn't change at remote process
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandle("kernel32.dll"),"LoadLibraryA");
	
	// Open remote process
	DWORD PID = GetProcessId("notepad.exe");
	proc = OpenProcess(PROCESS_ALL_ACCESS,FALSE,PID);
	
	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
	PVOID memAddr = (PVOID)VirtualAllocEx(proc,NULL,strlen(buffer)+1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	
	if (NULL == memAddr) {
		err = GetLastError();
		return 0;
	}
	// Write DLL name to remote process memory
	BOOL check = WriteProcessMemory(proc,memAddr,buffer,strlen(buffer)+1,NULL);
	if (0 == check) {
		err = GetLastError();
		return 0;
	}
	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	//create a new thread in notepad process 
	HANDLE hRemote = CreateRemoteThread(proc,NULL,0, (LPTHREAD_START_ROUTINE)LoadLibrary,memAddr,0,NULL);
	if (NULL == hRemote) {
		err = GetLastError();
		return 0;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	printf("Done!");
	return 0;
}
/*
Get the PID of the process 
Input:application name
Output:PID
*/
DWORD GetProcessId(LPCSTR szExeName)

{
	DWORD dwRet = 0;
	DWORD dwCount = 0;

	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32 pe = { 0 };
		pe.dwSize = sizeof(PROCESSENTRY32);

		BOOL bRet = Process32First(hSnapshot, &pe);

		while (bRet)
		{
			//Compare the name of each application
			if (!strcmp(szExeName, pe.szExeFile))
			{
				dwCount++;
				dwRet = pe.th32ProcessID;
			}
			bRet = Process32Next(hSnapshot, &pe);
		}

		if (dwCount > 1)
			dwRet = 0xFFFFFFFF;

		CloseHandle(hSnapshot);
	}

	return dwRet;
}

